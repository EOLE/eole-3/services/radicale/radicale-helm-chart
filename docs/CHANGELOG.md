# Changelog

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/radicale-helm-chart/compare/release/1.1.0...release/1.2.0) (2024-01-22)


### Features

* new stable for app version 1.1.0 ([d80a717](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/radicale-helm-chart/commit/d80a717b6852cd602bc836fc0b697294b252d395))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/radicale-helm-chart/compare/release/1.0.1...release/1.1.0) (2023-11-08)


### Features

* update chart to stable version 1.0.2 ([f704c45](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/radicale-helm-chart/commit/f704c45b922d64ddceebafa75b0113b3ecfcccc9))

### [1.0.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/radicale-helm-chart/compare/release/1.0.0...release/1.0.1) (2023-05-09)


### Bug Fixes

* helm testing deploy testing app version ([be317a7](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/radicale-helm-chart/commit/be317a79a3e7bd034b327790df21a0c770194d9e))
* publish new helm stable version ([25b001d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/radicale-helm-chart/commit/25b001d33e2e13a5980bc52e5715faa4ab776adc))

## 1.0.0 (2022-12-09)


### Features

* **ci:** build helm package ([30dc9f8](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/commit/30dc9f881fd8b0ea8d2c10b6173539eca51dab1f))
* **ci:** push helm package to chart repository ([b53d2d0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/commit/b53d2d0ca00fafedeece84d21261911e3b36cee0))
* **ci:** update helm chart version with `semantic-release` ([5ee0cd9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/commit/5ee0cd959bec21b5819bc81cc9a403d6cd5eed15))
* **ci:** validate helm chart at `lint` stage ([ba66578](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/commit/ba66578ab6caed87e55edabc7cc72f1172ee7e8e))
* use chart.yaml as default version ([1312578](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/commit/131257819121c1dedd707d4c5fb0ff248cadf2f3))


### Bug Fixes

* **radicale:** add containerport default value ([b0f55b3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/commit/b0f55b3f0f80aecc809367c188216764ce8c46b6))
* **radicale:** add image repository default value ([2167a92](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/commit/2167a92af13ffe936c68c0a71b2edc92d1f3981d))
* update hpa api version removed on k8s 1.26 ([926f421](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/commit/926f421d96247de392e40670d533dd948163a1d1))


### Continuous Integration

* **commitlint:** enforce commit message format ([fe634f9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/commit/fe634f91796d5c9aa7953f068e1a8cae0bd9fa36))
* **release:** create release automatically with `semantic-release` ([d19a596](https://gitlab.mim-libre.fr/EOLE/eole-3/services/radicale/commit/d19a596d0c1943c360d67ca90283da0aeb651ae7))
